# BASH AUR Downloader

## Purpose

This BASH script is intended on making the AUR download process easier and more
streamlined.

## Download

    cd ~/
    git clone https://bitbucket.org/mdill/bash_aurdownload.git

## Placement

It is suggested that this script be placed in `/usr/local/bin/` for simplicity
and ease-of-use.  Once this is done, it can be made executable by:

    $ chmod +x /usr/local/bin/aurdownload

## License

This project is licensed under the BSD License - see the [LICENSE.md](LICENSE.md) file for
details.

